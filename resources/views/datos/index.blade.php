@extends('layouts.app')

@section('content')


<div class="container">
    <div class="card">
        <div class="card-body">
             <div class="row justify-content-center">
                 <div class="col-md-8">
                     <div class="bg-info text-white">
                          <center><h1>Datos personales</h1></center>   
                     </div>
                   <table class="table table-striped table-bordered">
                     <thead>
                          <tr>
                           <th scope="col">ID</th>
                           <th scope="col">Nombre</th>
                           <th scope="col">Apellido paterno</th>
                           <th scope="col">Apellido materno</th>
                          <th scope="col">Fecha de nacimiento</th>
                          <th scope="col"><center>Acciones</center> </th>
                          </tr>
                        </thead>
                        @foreach($dato as $cate)
                          <tr>
                          <td>{{$cate->id}}</td>
                          <td>{{$cate->nombre}}</td>
                          <td>{{$cate->apellidop}}</td>
                           <td>{{$cate->apellidom}}</td>
                           <td>{{$cate->fechan}}</td>
                           <td>
                           <div class="row">
                           <div class="col-lg-6">
                           <a href="{{url('/datos/'.$cate->id.'/edit')}}"class="btn btn-link">Editar</a>
                           </div>
                           <div class="col-lg-6">
                           @include('datos.delete',['$dato' => $cate])
                           </div>
                           </div>
                           
                       
                           </td>
                           </tr> 
                       @endforeach
                    </table>
                    <div class="row justify-content-end">
                <form action="{{route ('datos.create')}}" method="GET">
                 <input class="btn btn-info text-white" type="submit" value="Nuevo">
                 </form>
                </div>
                </div>        
            </div>
        </div> 
     </div>
</div>
    
@endsection
