@extends('layouts.app')

@section('content')
<div class="container">
         <div class="card">
               <div class="card-body">
                <form action="{{route('datos.update',$dato->id)}}" method="POST">
                {{method_field('PATCH')}}
                  @csrf
              <div class="row justify-content-center form-group" >
                <h1>Registro de datos</h1>
              </div>
                <div class="row justify-content-center form-group">
                    <div class="col-lg-6">    
                        <input class="form-control" type="text" value="{{$dato->nombre}}" name="nombre" placeholder="Nombres">
                    </div>
                    <div class="col-lg-6">
                        <input class="form-control" type="text" value="{{$dato->apellidop}}" name="apellidop" placeholder="Apellido paterno">    
                     </div>
                 <div>
                 <br>
                 <br>
             </div class="row justify-content-center form-group"  >
                   <div class="col-lg-6">
                       <input class="form-control" type="text"value="{{$dato->apellidom}}" name="apellidom" placeholder="Apellido materno">
                  </div>
                  <div class="col-lg-6">
                       <input class="form-control" type="date" value="{{$dato->fechan}}" name="fechan" placeholder="Fecha de nacimiento">
                   </div>
              </div>
              <div class="row justify-content-end">
              <input class="btn btn-success" type="submit" value="Guardar"> 
              </div>
            </form>
            </div>
    </div>
</div>
@endsection
